#include "ResourceManager.h"

// get the caches
TextureCache ResourceManager::m_textureCache;

// get the data from the caches
glTexture ResourceManager::getTexture(std::string textureFilePath) {
	return m_textureCache.getTexture(textureFilePath);
}
