#pragma once
#include <vector>

class IApplication;
class IApplicationScreen;

class ScreenList {

public:

	ScreenList(IApplication* application);
	~ScreenList();

	// move to next/previous screen in vector
	IApplicationScreen* moveToNext();
	IApplicationScreen* moveToPrevious();

	void setScreen(int newSceneIndex);
	void addScreen(IApplicationScreen* newScreen);
	IApplicationScreen* getCurrentScreen();

	// iterate over screens and destroy them
	// also resizes vector to 0 and sets current index to NO_SCREEN_INDEX
	void destroy();

protected:
	
	int m_currentIndex = -1;
	std::vector<IApplicationScreen*> m_screens;
	IApplication* m_application = nullptr;
};