#pragma once

#include <glm\glm\glm.hpp>
#include <glm\glm\gtc\matrix_transform.hpp>


class Camera3D {

public:
	Camera3D();
	~Camera3D();

	// init the camera with variables and screen dimensions
	void init(
		float screenWidth,
		float screenHeight,
		glm::vec3 position,
		glm::vec3 worldUp,
		glm::vec3 front,
		float yaw,
		float pitch,
		float zoom
	);

	// convert coordinates from screen to 3D game world
	glm::vec3 convertScreenToWorld(glm::vec2 coordPair);

	// getters
	glm::mat4 getViewMatrix() { return glm::lookAt(m_position, m_position + m_front, m_camUp); }
	glm::vec3 getFront() { return m_front; }
	glm::vec3 getRight() { return m_right; }
	glm::vec3 getPosition() { return m_position; }
	float getZoom() { return m_zoom; }

	// setters
	void setPosition(glm::vec3 newPosition) { m_position = newPosition; }
	void setYaw(float newYaw) { m_yaw = newYaw; }
	void setPitch(float newPitch) { m_pitch = newPitch; }
	void setZoom(float newZoom) { m_zoom = newZoom; }

	// incrementors
	void incrementYaw(float increment) { m_yaw += increment; }
	void incrementPitch(float increment) { m_pitch += increment; }
	void incrementZoom(float increment) { m_zoom += increment; }

	// update camera vectors given orientation
	void calculateCameraVectors();

private:

	// camera vectors
	glm::vec3 m_position;
	glm::vec3 m_front;
	glm::vec3 m_right;
	glm::vec3 m_camUp;
	glm::vec3 m_worldUp;

	// camera properties
	float m_yaw;
	float m_pitch;
	float m_screenHeight;
	float m_screenWidth;
	float m_zoom;

};
