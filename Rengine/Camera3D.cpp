#include "Camera3D.h"


Camera3D::Camera3D() {
	// empty
}


Camera3D::~Camera3D() {
	// empty
}

void Camera3D::init(float screenWidth, float screenHeight, glm::vec3 position, glm::vec3 worldUp, glm::vec3 front, float yaw, float pitch, float zoom) {

	// set camera variables
	m_position = position;
	m_worldUp = worldUp;
	m_front = front;
	m_yaw = yaw;
	m_pitch = pitch;
	m_zoom = zoom;
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
}

glm::vec3 Camera3D::convertScreenToWorld(glm::vec2 coordPair) {

	// invert y axis
	coordPair.y = m_screenHeight - coordPair.y;

	// set origin to center of screen
	coordPair -= glm::vec2(m_screenWidth / 2, m_screenHeight / 2);

	// account for zoom
	coordPair /= m_zoom;

	// account for camera position in world
	coordPair.x += m_position.x;
	coordPair.y += m_position.y;

	// return vec 3 of coords with z = camera zposition
	return glm::vec3(coordPair.x, coordPair.y, m_position.z);

}

void Camera3D::calculateCameraVectors() {

	// updated front vector
	glm::vec3 newFront;

	// calculate each component of front vector
	// (seperate lines to help debugging)
	newFront.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
	newFront.y = sin(glm::radians(m_pitch));
	newFront.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));

	// set the front vector to the newly calculated (and normalized) front vec
	m_front = glm::normalize(newFront);

	// recalculate right/up vectors
	m_right = glm::normalize(glm::cross(m_front, m_worldUp));
	m_camUp = glm::normalize(glm::cross(m_right, m_front));

}
