#pragma once

#include <string>
#include <SDL/SDL.h>

class Window
{
public:
	Window();
	~Window();

	int init(const std::string& windowName, const int& windowWidth, const int& windowHeight);

	// getters
	int getWindowWidth() { return m_windowWidth; }
	int getWindowHeight() { return m_windowHeight; }
	SDL_Window* getWindow() { return m_window; }

	// setters
	void setWindowWidth(const int& newWidth) { m_windowWidth = newWidth; }
	void setWindowHeight(const int& newHight) { m_windowHeight = newHight; }

	void swapBuffers() { SDL_GL_SwapWindow(m_window); }

private:
	int m_windowWidth = 800;
	int m_windowHeight = 600;
	std::string m_windowName;
	SDL_Window* m_window;
};