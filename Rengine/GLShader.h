#pragma once
#include <GL\glew.h>

enum class RENGINE_SHADER_TYPE {FRAGMENT, VERTEX};


class GLShader {

public:
	GLShader();
	~GLShader();

	// creates shaderID with opengl
	void init(RENGINE_SHADER_TYPE shaderType);

	// compiles shader source
	void compile(const char* shaderSource);

	// getters
	GLuint getShaderID() { return m_shaderID; }
	RENGINE_SHADER_TYPE getType() { return m_shaderType; }

private:
	GLuint m_shaderID;
	RENGINE_SHADER_TYPE m_shaderType;
};
