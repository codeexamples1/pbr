#pragma once
#include <string>
#include <cstdlib>
#include <iostream>

#include <SDL\SDL.h>


extern void fatalError(std::string errorString) {

	// display error string
	std::cout << errorString << std::endl;
	std::cout << "Press any key to end...";

	// wait for key input to escape application
	int temp;
	std::cin >> temp;
	SDL_Quit();
	exit(69);
}