#pragma once
#include "Window.h"
#include "ScreenList.h"
#include "InputManager.h"

#include <memory>

class IApplication
{
public:
	IApplication();
	virtual ~IApplication();

	// runs game and starts game loop
	void runApp();

	// exits the game and calls clean-up functions
	void exitApp();

	// game-specific initialization logic
	virtual void onInit() = 0;

	// setup screens for main game
	virtual void addScreens() = 0;

	// game-specific logic when exiting the game
	virtual void onExit() = 0;

	// get the game's FPS
	const float getFPS() const {
		return m_fps;
	}

	void onSDLEvent(SDL_Event& evnt);

	InputManager inputManager;

protected:

	// functions only needed by the game
	void update(float deltaTime);

	void draw();

	// functions to initialize the game
	bool initApp();
	bool initSystems();

	// current fps
	float m_fps = 0;

	// smart pointer to list of game screens
	std::unique_ptr<ScreenList> m_screenList = nullptr;

	// currently running screen
	IApplicationScreen* m_currentScreen = nullptr;

	// is the game running
	bool m_isRunning = false;

	// window and initial values
	Window m_window;
	int m_screenWidth = 800;
	int m_screenHeight = 600;
	std::string m_appName = "default";

};