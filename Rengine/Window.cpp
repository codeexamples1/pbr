#include "Window.h"
#include "RengineErrors.h"

#include <GL/glew.h>

Window::Window() {
	// empty
}

Window::~Window() {
	// empty
}

int Window::init(const std::string& windowName, const int& windowWidth, const int& windowHeight)
{
	m_windowName = windowName;
	m_windowWidth = windowWidth;
	m_windowHeight = windowHeight;

	m_window = SDL_CreateWindow(
		m_windowName.c_str(),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		m_windowWidth,
		m_windowHeight,
		SDL_WINDOW_OPENGL
	);

	if (m_window == nullptr) {
		fatalError("SDL window could not be created.");
	}

	SDL_GLContext glContext = SDL_GL_CreateContext(m_window);
	if (glContext == nullptr) {
		fatalError("OpenGL context could not be created! Exiting.");
	}

	GLenum glewInitResult = glewInit();
	if (glewInitResult != GLEW_OK) {
		fatalError("GLEW could not be initialized! Exiting.");
	}

	glClearColor(0.1f, 0.17f, 0.17f, 1.0f);
	SDL_GL_SetSwapInterval(0);
	
	// enable alpha blending
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	return 0;
}
