#pragma once
#include <GL\glew.h>
#include <string>


struct glTexture {
	std::string filePath = "";
	GLuint id;
	int width, height;
};
