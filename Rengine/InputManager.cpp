#include "InputManager.h"

InputManager::InputManager() : m_mouseCoords(0.0f) {
	// empty
}

InputManager::~InputManager() {
	// empty
}

void InputManager::update() {

	// iterate through current keymap
	// set previous key map equal to current key map to check against next frame
	for (auto& it : m_keyMap) {
		m_previousKeyMap[it.first] = it.second;
	}
}

void InputManager::pressKey(unsigned int keyID) {
	m_keyMap[keyID] = true;
}

void InputManager::releaseKey(unsigned int keyID) {
	m_keyMap[keyID] = false;
}

void InputManager::setMouseCoords(float x, float y) {
	// set previous mousecoords and update the current coords
	m_previousMouseCoords = m_mouseCoords;
	m_mouseCoords.x = x;
	m_mouseCoords.y = y;
}

bool InputManager::isKeyDown(unsigned int keyID) {
	// create iterator to find keyID in map
	auto it = m_keyMap.find(keyID);
	if (it != m_keyMap.end()) {
		return it->second;
	}
	else {
		return false;
	}
}

bool InputManager::isKeyPressed(unsigned int keyID) {

	// check if key is down this frame but not last frame
	if (isKeyDown(keyID) == true && wasKeyDown(keyID) == false) {
		return true;
	}
	return false;
}

bool InputManager::wasKeyDown(unsigned int keyID) {
	auto it = m_previousKeyMap.find(keyID);
	if (it != m_previousKeyMap.end()) {
		return it->second;
	}
	else {
		return false;
	}
}