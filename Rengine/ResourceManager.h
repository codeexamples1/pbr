#pragma once
#include "GLObjects.h"
#include "TextureCache.h"
#include <string>

class ResourceManager {

public:

	// load the texture/scenes from file or fetch from cache
	static glTexture getTexture(std::string textureFilePath);

private:
	static TextureCache m_textureCache;
};
