#pragma once

class FPSLimiter {

public:
	FPSLimiter();
	~FPSLimiter();

	void init(float maxFPS);

	// grab the start ticks
	void beginFrame();

	// returns the fps
	float end();

	// setters
	void setMaxFPS(float maxFPS);

private:

	// calculate the fps
	void calculateFPS();
	float m_fps;

	// time difference between current/previous frame ticks
	float m_frameTime;

	// max fps for game
	float m_maxFPS;

	// ticks since SDL initialization
	unsigned int m_startTicks;

};