#include "GLShader.h"
#include "RengineErrors.h"

#include <vector>


GLShader::GLShader() {
	// empty
}


GLShader::~GLShader() {
	// empty
}

void GLShader::init(RENGINE_SHADER_TYPE shaderType) {

	m_shaderType = shaderType;

	switch (m_shaderType) {
	case RENGINE_SHADER_TYPE::VERTEX:
		m_shaderID = glCreateShader(GL_VERTEX_SHADER);
		break;
	case RENGINE_SHADER_TYPE::FRAGMENT:
		m_shaderID = glCreateShader(GL_FRAGMENT_SHADER);
		break;
	default:
		fatalError("Shader type not supported! Ending.");
		break;
	}

	if (m_shaderID == 0) {
		fatalError("Fragment shader could not be created. Ending.");
	}

}

void GLShader::compile(const char* shaderSource) {

	// check the shader was initialised correctly
	if (m_shaderID == 0) {
		fatalError("Shader was not initialised correctly. Was init() called?");
	}

	// use source to generate a shader
	glShaderSource(m_shaderID, 1, &shaderSource, nullptr);

	// compile the shader and check success
	glCompileShader(m_shaderID);
	GLint compilationSuccess = 0;
	glGetShaderiv(m_shaderID, GL_COMPILE_STATUS, &compilationSuccess);

	// if unsuccessful compilation, handle the error
	if (compilationSuccess == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(m_shaderID, GL_INFO_LOG_LENGTH, &maxLength);
		std::vector<char> errorLog(maxLength);
		glGetShaderInfoLog(m_shaderID, maxLength, &maxLength, &errorLog[0]);

		// Don't leak the shader.
		glDeleteShader(m_shaderID); 

		// print error log and quit
		std::printf("%s\n", &(errorLog[0]));
		fatalError("Shader failed to compile");
	}
}
