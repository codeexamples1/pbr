#pragma once

#include "RenderScreen.h"

#include <Rengine/IApplication.h>
#include <memory>

class PBRApplication : public IApplication
{
	
public:

	PBRApplication();
	~PBRApplication();

	virtual void onInit() override;
	virtual void addScreens() override;
	virtual void onExit() override;

private:

	// smart pointer to gameplay screen for app
	// initialized to null for easier debugging
	std::unique_ptr<RenderScreen> m_renderScreen = nullptr;


};
