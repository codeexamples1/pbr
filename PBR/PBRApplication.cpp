#include "PBRApplication.h"

#include <Rengine/ScreenList.h>
#include <iostream>
#include <SDL\SDL.h>


PBRApplication::PBRApplication() {
	// empty
}


PBRApplication::~PBRApplication() {
	// empty
}

void PBRApplication::onInit() {
	// empty
}

void PBRApplication::addScreens() {
	// add and set render screen
	m_renderScreen = std::make_unique<RenderScreen>(&m_window);
	m_screenList->addScreen(m_renderScreen.get());
	m_screenList->setScreen(m_renderScreen->getIndex());
}

void PBRApplication::onExit() {
	// empty
}
