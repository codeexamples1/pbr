#include <Rengine/IApplication.h>

#include "PBRApplication.h"

int main(int argc, char** argv) {
	PBRApplication application;
	application.runApp();
	return 0;
}