#include "RenderScreen.h"
#include "ScreenIndicesh.h"
#include <Rengine/IApplication.h>
#include <iostream>
#include <GL/glew.h>

RenderScreen::RenderScreen(Window* window) : m_window(window) {
	m_index = SCREEN_INDEX_RENDER;
}


RenderScreen::~RenderScreen() {
	// empty
}


int RenderScreen::getNextIndex() const {
	return NO_SCREEN_INDEX;
}

int RenderScreen::getPrevIndex() const {
	return NO_SCREEN_INDEX;
}

void RenderScreen::build() {
	// empty
}


void RenderScreen::destroy() {
	// TODO: implement
}

void RenderScreen::onEntry() {
	glEnable(GL_DEPTH_TEST);
	SDL_ShowCursor(1);
	initUI();
}

void RenderScreen::onExit() {

	// clean up initialized subsystems before exiting application
	SDL_Quit();
	exit(69);
}

void RenderScreen::update(float deltaTime) {
	checkInput();
}

void RenderScreen::draw() {
	// clear the previously drawn screen
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


void RenderScreen::initUI() {
	// TODO: implement
}

void RenderScreen::checkInput() {
	SDL_Event evnt;
	while (SDL_PollEvent(&evnt)) {
		m_application->onSDLEvent(evnt);
	}
}
