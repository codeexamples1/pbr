#pragma once

#include <Rengine/IApplicationScreen.h>
#include <Rengine/Camera3D.h>
#include <Rengine/GLSLProgram.h>
#include <Rengine/Window.h>
#include <GL/GL.h>
#include <GL/glew.h>


// main gameplay screen for 3D Breakout
class RenderScreen : public IApplicationScreen
{
public:

	RenderScreen(Window* window);
	~RenderScreen();

	// get next/previous screen index
	virtual int getNextIndex() const override;
	virtual int getPrevIndex() const override;

	// build, destroy, onEntry and onExit functions:
	// all for calling unique screen logic at different points of its lifetime

	// call at start of application
	virtual void build() override;

	// call at end of application
	virtual void destroy() override;

	// call when entering screen to perfrom initialization
	virtual void onEntry() override;

	// call when exiting a screen to perform clean up
	virtual void onExit() override;

	// call in main loop
	virtual void update(float deltaTime) override;
	virtual void draw() override;

private:

	void initUI();

	// check for user input
	void checkInput();

	// sets up player based on profile
	void configurePlayer(std::string username);

	// window to draw to
	Window* m_window;
	Camera3D m_camera;

	int m_nextScreenIndex;

};
